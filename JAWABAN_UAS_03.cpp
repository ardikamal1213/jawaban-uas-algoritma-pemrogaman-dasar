#include <iostream>

int main() {
    int i, jumlah_mahasiswa;
    double nilai[100], quiz[100], absen[100], uts[100], uas[100], tugas[100];
    char Huruf_Mutu[100];

    std::cout << "Masukkan jumlah mahasiswa: ";
    std::cin >> jumlah_mahasiswa;

    for (i = 0; i < jumlah_mahasiswa; i++) {
        std::cout << "\nMahasiswa ke-" << i+1 << "\n";
        std::cout << "Absen = ";
        std::cin >> absen[i];
        std::cout << "Tugas = ";
        std::cin >> tugas[i];
        std::cout << "Quiz = ";
        std::cin >> quiz[i];
        std::cout << "UTS = ";
        std::cin >> uts[i];
        std::cout << "UAS = ";
        std::cin >> uas[i];

        nilai[i] = ((0.1 * absen[i]) + (0.2 * tugas[i]) + (0.3 * quiz[i]) + (0.4 * uts[i]) + (0.5 * uas[i])) / 2;

        if (nilai[i] > 85 && nilai[i] <= 100)
            Huruf_Mutu[i] = 'A';
        else if (nilai[i] > 70 && nilai[i] <= 85)
            Huruf_Mutu[i] = 'B';
        else if (nilai[i] > 55 && nilai[i] <= 70)
            Huruf_Mutu[i] = 'C';
        else if (nilai[i] > 40 && nilai[i] <= 55)
            Huruf_Mutu[i] = 'D';
        else if (nilai[i] >= 0 && nilai[i] <= 40)
            Huruf_Mutu[i] = 'E';

        std::cout << "Huruf Mutu : " << Huruf_Mutu[i] << "\n";
    }
    return 0;
}